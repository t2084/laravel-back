<?php

use App\Http\Controllers\CancionesController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\GitLabController;
use App\Http\Controllers\UsuariosController;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('usuarios')->group( function() {
    Route::get('/', function () { return Usuarios::all(); });
    Route::post('/nuevoUsuario/{name}/{email}/{password}/{edad}/{pais}/{sexo}', function ( Request $request, $name, $email, $password, $edad, $pais, $sexo )
    {
        return UsuariosController::crearUsuario( $name, $email, $password, $edad, $pais, $sexo );
    });
    Route::post('/eliminarUsuario/{email}', function ( Request $request,  $email )
    {
        return UsuariosController::borrarUsuario( $email );
    });
    Route::get('/encontrarUsuario/{email}/{password}', function( Request $request, $email, $password ) {
        return UsuariosController::buscarUsuarioEmailPassword ( $email, $password );
    });
    Route::get('/encontrarUsuario/{email}', function( Request $request, $email ) { return UsuariosController::buscarUsuarioEmail ( $email ); });
    Route::get('/encontrarUsuarioId/{id}', function( Request $request, $id ) { return UsuariosController::buscarUsuarioid ( $id ); });
    Route::post('/editarUsuario/{email}/{nuevoNombre}/{nuevoPassword}', function( Request $request, $email,
                                                                                          $nuevoNombre, $nuevoPassword) {
        return UsuariosController::editarUsuario( $request->route('email'), $nuevoPassword, $nuevoNombre );
    });
    Route::post('/editarAvatarUsuario/{email}/{avatar}', function(Request $request, $email) {
        return UsuariosController::editarAvatarusuario ( $email, $request->request->get('foto') );
    });
    Route::get('getUsuario/{query}', function (Request $request, $query) { return UsuariosController::buscarUsuario( $query ); });
});

Route::prefix('canciones')->group( function () {
    Route::post('/addCancion/{id}/{nombre}/{duracion}/{artista}/{genero}', function(Request $request, $songId, $nombre, $duracion, $artista, $genero) {
        return CancionesController::crearCancion( $songId, $nombre, $duracion, $genero, $artista );
    });

    Route::post('/canciongustada/{cancion}/{email}/{valoracion}', function ( Request $request, $cancion, $email, $valoracion ) {
        return CancionesController::gustarCancion ( $email, $cancion, $valoracion );
    });

    Route::post('/cancionnogustada/{cancion}/{email}/{valoracion}', function ( Request $request, $cancion, $email, $valoracion ) {
        return CancionesController::desgustarCancion( $email, $cancion, $valoracion );
    });

    Route::get('/cancionesgustadas/{email}', function ( Request $request, $email ) {
        return CancionesController::cancionesGustadas ( $email );
    });

    Route::get('/escanciongustada/{id}/{email}', function ( Request $request, $id, $email ) {
       return CancionesController::esCancionGustada( $id, $email );
    });

    Route::get('getCancion/{query}', function (Request $request, $query) {
        return CancionesController::obtenerCancion ( $query );
    });
});

Route::prefix('tablas')->group( function () {
    Route::get('/listartablas', function () { return DatabaseController::listarTablas(); });
    Route::get('/listarCampos/{tabla}', function(Request $request, $tabla) {
        return DatabaseController::listarCamposTablas( $tabla);
    });

    Route::get('/listarRegistros/{tabla}/{skip}/{offset}', function(Request $request, $tabla, $skip, $offset) {
        return DatabaseController::listarRegistrosTabla( $tabla, $skip, $offset );
    });

    Route::post('/establecerImportanciaTablas/{tabla}/{campo}/{importancia}',
        function (Request $request, $tabla, $campo, $importancia) {
        return DatabaseController::establecerImportanciaTablaRegistro( $tabla, $campo, $importancia );
    });

});

Route::prefix('repositorio')->group( function() {
    Route::get('checkRepoUpdated', function() {
        return GitLabController::checkRepoUpdated();
    });

    Route::get('updateRepo', function() {
        return GitLabController::updateRepo();
    });

    Route::get('updateData' , function() {
        GitLabController::updateData();
    });
});

Route::prefix('spotify')->group( function() {

});






