<?php

namespace Database\Factories;

use App\Models\Usuarios;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsuariosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Usuarios::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'userId' => $this->faker->randomNumber(),
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail(),
            'password' => $this->faker->password,
            'age' => $this->faker->randomNumber(),
            'sex' => $this->faker->randomElement(['female', 'male']),
            'country' => $this->faker->countryISOAlpha3(),
            'isAdmin' => 0,
            'avatar' => $this->faker->imageUrl
        ];
    }
}
