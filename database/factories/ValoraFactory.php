<?php

namespace Database\Factories;

use App\Models\Valora;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class ValoraFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Valora::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition()
    {
        return [
            'liked' => $this->faker->boolean(),
            'songId' => DB::table('canciones')->select('songId')->inRandomOrder()->first()->songId,
            'userId' => DB::table('usuarios')->select('userId')->inRandomOrder()->first()->userId
        ];
    }
}
