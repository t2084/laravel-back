<?php

namespace Database\Factories;

use App\Models\Canciones;
use Illuminate\Database\Eloquent\Factories\Factory;

class CancionesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Canciones::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'songId' => $this->faker->randomNumber(),
            'songId' => $this->faker->text,
            'title' => $this->faker->title,
            'artistId' => $this->faker->randomNumber(),
            // 'artistId' => random_int(DB::table('Artista')->min('id'), DB::table('Artista')->max('id'))
            'genre' => $this->faker->text(),
            'duration' => $this->faker->randomNumber()
        ];
    }
}
