<?php

namespace Database\Seeders;

use App\Models\Canciones;
use App\Models\Usuarios;
use App\Models\Valora;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        Usuarios::factory(
            ['name' => 'Marta',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('0707'),
                'age' => 21,
                'sex' => 'female',
                'country' => 'ES',
                'isAdmin' => 1
                ]
        )->create();

        Usuarios::factory(
            ['name' => 'Marta',
                'email' => 'mfernandez@seyte.com',
                'password' => Hash::make('0707'),
                'age' => 21,
                'sex' => 'female',
                'country' => 'ES',
                'isAdmin' => 0
            ]
        )->create();

        // Usuarios::factory(30000)->create();

    }
}
