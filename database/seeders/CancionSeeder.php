<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CancionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        DB::table('canciones')->insert([
            'songId' => $faker->text(),
            'title' => $faker->title,
            'artistId' => $faker->randomNumber(),
            'genre' => $faker->text(),
            'duration' => $faker->randomNumber()
        ]);
    }
}
