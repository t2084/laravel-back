<?php

namespace Database\Seeders;

use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        DB::table('usuarios')->insert([
            'userId' => $faker->randomNumber(),
            'name' => $faker->name(),
            'age' => $faker->randomNumber(),
            'sex' => $faker->randomElement(['M', 'F']),
            'country' => $faker->countryISOAlpha3,
            'email' => $faker->email,
            'password' => $faker->password,
            'avatar' => $faker->imageUrl
        ]);
    }
}
