<?php

namespace Database\Seeders;

use Exception;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ValoraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        $faker = Factory::create();
        DB::table('valora')->insert([
            'liked' => $faker->boolean(),
            'songId' => DB::table('canciones')->select('songId')->inRandomOrder()->first()->songId,
            'userId' => DB::table('usuarios')->select('userId')->inRandomOrder()->first()->userId
        ]);
    }
}
