<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReCreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->integer('userId')->autoIncrement()->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->integer('age');
            $table->string('sex')->default(NULL);
            $table->string('country')->default(NULL);
            $table->boolean('isAdmin')->default(false);
            $table->string('avatar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
