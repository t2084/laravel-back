cd /tfg || exit 1

if [ -d "laravel-back" ]; then
  cd laravel-back || exit 1
  echo "Updating the repositorty ... "
  git pull origin master
  git submodule update --recursive --remote
  echo "Repository updated"
  cd laravel-back || exit 1
  echo "Updating changes in docker images and rebuilding the app ... "
  sudo make rebuild
  echo "All changed done correctly"
else
  echo "Cloning the repository ... "
  git clone --recursive git@gitlab.com:t2084/laravel-back.git
  echo "Repository cloned"
  cd laravel-back || exit 1
  echo "Building docker image ... "
  sudo make install_dev
  echo "Docker image built and app deployed"
fi

