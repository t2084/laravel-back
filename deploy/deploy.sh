echo "Desplegando la aplicación en AWS ..."
export COMPOSE_INTERACTIVE_NO_CLI=1

if [ "$#" -ne 2 ]; then
    echo "Two params are needed. Received: $#"
    exit
fi

DEPLOY_SERVER_IP=$1
PROJECT_NAME=$2

ssh -oStrictHostKeyChecking=no ubuntu@"$DEPLOY_SERVER_IP" 'bash -s' < deploy/ini.sh "$PROJECT_NAME"


