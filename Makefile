dev:
	docker-compose up -d
logs:
	docker-compose up -d && docker-compose logs
install_dev:
	@make rebuild
	@make init
	@make package
init:
	docker-compose up -d --build
	docker-compose exec -T php composer install
	docker-compose exec -T php cp .env.example .env
	docker-compose exec -T php php artisan key:generate
	docker-compose exec -T php php artisan storage:link
	docker-compose exec -T php php artisan migrate
	docker-compose exec -T php php artisan db:seed
	chmod +x iniCleanData.bash
	docker-compose exec -T php bash "./iniCleanData.bash"
	chmod +x recommenderSystem.bash
	docker-compose exec -T php bash "./recommenderSystem.bash"
rebuild:
	docker-compose build --no-cache --force-rm
down:
	docker-compose down
stop:
	docker-compose stop
migrate:
	docker-compose exec php php artisan migrate
rollback:
	docker-compose exec php php artisan migrate:rollback
fresh:
	docker-compose exec php php artisan migrate:fresh --seed
seed:
	docker-compose exec php php artisan db:seed
php:
	docker-compose exec php bash
mysql:
	docker-compose exec mysql bash
package:
	docker-compose exec php bash "./recommenderSystem.bash"

