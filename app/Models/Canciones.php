<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Canciones extends Model
{
    use HasFactory;

    protected $primaryKey = 'songId';

    public $incrementing = true;

    protected $keyType = 'string';

    protected $fillable = [
      'songId',
      'title',
      'artistId',
      'genre',
      'duration'
    ];
}
