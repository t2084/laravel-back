<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Valora extends Model
{
    use HasFactory;

    public $table = "valora";

    protected $primaryKey = 'valorationId';

    public $incrementing = true;

    protected $keyType = 'int';

    protected $fillable = [
        'valorationId',
        'songId',
        'userId',
        'valoration'
    ];
}
