<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Usuarios extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * @var string the primary key associated with the table
     */
    protected $primaryKey = 'userId';

    /**
     * @var bool the primary key is not an incrementing numeric field
     */
    public $incrementing = true;

    /**
     * @var string the primary key is a string
     */
    protected $keyType = 'int';

    protected $fillable =  [
        'userId',
        'name',
        'age',
        'sex',
        'country',
        'email',
        'password',
        'api_token'
    ];

    protected $hidden = [
      'remember_password',
        'api_token'
    ];

    protected $casts = [
      'email_verified_at' => 'datetime',
    ];

}
