<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatabaseController extends Controller
{

    /**
     * @return array
     */
    public static function listarTablas ( ) {
        return DB::select("SHOW TABLES");
    }

    /**
     * @param $tabla
     * @return mixed
     */
    public static function listarCamposTablas ( $tabla ) {
        return DB::getSchemaBuilder()->getColumnListing("ml_clean" . strtoupper($tabla[0]) . substr($tabla, 1));
    }

    /**
     * @param $tabla
     * @param $skip
     * @param $offset
     * @return array
     */
    public static function listarRegistrosTabla ( $tabla, $skip, $offset ) {
        return DB::table("ml_clean" . strtoupper($tabla[0]) . substr($tabla, 1))
            ->skip($skip)
            ->take($offset)
            ->get()->toArray();
    }

    /**
     * @param string $tabla
     * @param string $registro
     * @param $importancia
     * @return bool|void
     */
    public static function establecerImportanciaTablaRegistro ( string $tabla, string $registro, $importancia ) {
        if ( str_contains($registro, "sex")) { $registro = "sex"; }
        $tabla = ucfirst($tabla);

        DB::table('ml_clean' . $tabla . 'Importance')
            ->where($registro, "!=", 0)
            ->delete();
        try {
            return DB::table ( 'ml_clean' . $tabla . 'Importance' )
                ->insert ([$registro => intval($importancia)]);
        } catch (\Exception $e ) {
            dd ( $e );
        }
    }
}
