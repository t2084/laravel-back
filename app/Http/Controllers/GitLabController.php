<?php

namespace App\Http\Controllers;

use Exception;

class GitLabController extends Controller{

    public static function updateData() {
        $output = [];
        $exitStatus = 0;
        try {
            exec("./updateData.bash", $output, $exitStatus);
        } catch (Exception $e) { }
    }

    public static function checkRepoUpdated() {
        $output = [];
        $exitStatus = 0;
        try {
            exec("./checkRepoUpdated.bash", $output, $exitStatus);
            return !str_contains($output[0], "up to date");
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function updateRepo() {
        $output = [];
        $exitStatus = 0;
        try {
            exec("./updateAlgRepo.bash", $output, $exitStatus);
            return str_contains($output[count($output) - 1 ], "Updating");
        } catch (Exception $e) {
            return $e;
        }
    }
}
