<?php

namespace App\Http\Controllers;

use App\Models\Canciones;
use App\Models\Usuarios;
use App\Models\Valora;
use Illuminate\Support\Facades\Auth;

class CancionesController extends Controller
{

    /**
     * @param $songId
     * @param $nombre
     * @param $duracion
     * @param $genero
     * @param $artista
     * @return bool|\Exception
     */
    public static function crearCancion ( $songId, $nombre, $duracion, $genero, $artista ) {
        try {
            return Canciones::query()->insert([
                "songId" => $songId,
                'title' => $nombre,
                "duration" => $duracion,
                "genre" => $genero,
                "artistId" => $artista
            ]);
        } catch (\Exception $ex) {
            return $ex;
        }
    }

    /**
     * @param $email
     * @param $cancion
     * @param $valoracion
     * @return bool
     */
    public static function gustarCancion ( $email, $cancion, $valoracion ) {
        $usuarioId = Usuarios::query()->where(['email' =>  $email])->get()->first()->userId;
        $count = Valora::query()->where([
            "songId" => $cancion,
            "userId" => $usuarioId,
            "liked" => 1
            ])->count();
        if ( $count > 0 ) return false;
        return Valora::query()->insert([
            "songId" => $cancion,
            "userId" => $usuarioId,
            "liked" => 1,
        ]);
    }

    /**
     * @param $email
     * @param $cancion
     * @param $valoracion
     * @return bool
     */
    public static function desgustarCancion ( $email, $cancion, $valoracion ): bool
    {
        $usuarioId = Usuarios::query()->where(['email' =>  $email])->get()->first()->userId;
        $cancion = Canciones::query()->where(['songId' => $cancion])->get()->first();

        $valoracion = Valora::query()->where(['songId' => $cancion->songId, 'userId' => $usuarioId])
            ->update(['liked' => 0]);

        if ( $valoracion <= 0 ) {
            return Valora::query()->insert([
                "songId" => $cancion,
                "userId" => $usuarioId,
                "liked" => $valoracion,
            ]);
        }

        return true;
    }

    /**
     * @param $email
     * @return array
     */
    public static function cancionesGustadas ( $email ): array
    {
        $usuarioId = Usuarios::query()->where(['email' => $email])->get()->first()->userId;
        $valoracionesUsuario = Valora::query()->where(['userId' => $usuarioId, "liked" => 1])->get()->toArray();
        $canciones = [];
        foreach ($valoracionesUsuario as $valoracion) {
            $canciones[] = $valoracion['songId'];
        }
        return $canciones;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function esCancionGustada ( $id, $email ): bool {
        $userId = Usuarios::query()->where(['email' => $email])->get()->first()->userId;
        $valoration = Valora::query()->where('songId', '=', $id )
            ->where('userId', '=', $userId)
            ->get()->first();

        if ( $valoration ) {
            return $valoration->liked == 1;
        }

        return false;
    }

    /**
     * @param $query
     * @return array
     */
    public static function obtenerCancion ( $query ): array
    {
        return Canciones::query()
            ->where('title','LIKE', '%' . $query . '%')->get()->toArray();
    }


}
