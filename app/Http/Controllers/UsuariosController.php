<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{

    /**
     * @param $name
     * @param $email
     * @param $password
     * @param $edad
     * @param $pais
     * @param $sexo
     * @return bool
     */
    public static function crearUsuario ( $name, $email, $password, $edad, $pais, $sexo ): bool
    {
        try {
            return Usuarios::query()->insert(
                [
                    'name' => $name,
                    'email' => $email,
                    'password' => Hash::make($password),
                    'age' => $edad,
                    'country' => $pais,
                    'sex' => $sexo,
                    'avatar' => ''
                ]);
        } catch ( \Exception $ex) {
            return false;
        }
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function borrarUsuario ( $email )
    {
        return Usuarios::query()->where(['email' => $email])->delete();
    }

    /**
     * @param $email
     * @param $password
     * @return mixed|null
     */
    public static function buscarUsuarioEmailPassword ( $email, $password )
    {
        $apiToken = new ApiTokenController();
        $user = Usuarios::query()->where(['email' => $email])->get()->first();
        if ( Hash::check($password, $user->password) ) {
            Auth::login($user);
            $apiToken->update( );
            return $user;
        }
        return null;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function buscarUsuarioEmail ( $email )
    {
        return Usuarios::query()->where(['email' => $email])->get()->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function buscarUsuarioid ( $id )
    {
        return Usuarios::query()->where(['userId' => $id])->get()->first();
    }

    /**
     * @param $email
     * @param $nuevoPassword
     * @param $nuevoNombre
     * @return int
     */
    public static function editarUsuario ( $email, $nuevoPassword, $nuevoNombre ): int
    {
        return Usuarios::query()->where(['email' => $email])->update([
            'password' => Hash::make($nuevoPassword),
            'name' => $nuevoNombre
        ]);
    }

    /**
     * @param $email
     * @param $foto
     * @return int
     */
    public static function editarAvatarusuario ( $email, $foto ): int
    {
        return Usuarios::query()->where(['email' => $email])->update([ 'avatar' => $foto ]);
    }

    /**
     * @param $query
     * @return array
     */
    public static function buscarUsuario ( $query ): array
    {
        return Usuarios::query()
            ->where('email','LIKE', '%' . $query . '%')
            ->orWhere('name','LIKE', '%' . $query . '%')->get()->toArray();
    }
}
