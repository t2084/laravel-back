<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;


/**
 * https://laravel.com/docs/5.8/api-authentication
 */
class ApiTokenController extends Controller
{
    /**
     * Update the authenticated user's API token.
     *
     * @return array
     */
    public function update(): array
    {
        $token = Str::random(60);
        auth()->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();
        return ['token' => $token];
    }

}
