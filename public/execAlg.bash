cd .. && cd machine-learning && cd RecommendationSystem || exit 1


java -jar target/marta-tfg-0.0.1-SNAPSHOT.jar "$1" "$2" "$3" "$4" -noverify -XX:TieredStopAtLevel=1 -XX:CICompilerCount=1

exitValue=$?

case $exitValue in
  0)
    exit $exitValue
    ;;
  1)
    printf "Ha ocurrido un error \n"
    exit $exitValue
    ;;
  2)
    printf "Hay un campo con valor no numérico \n"
    exit $exitValue
    ;;
  3)
    printf "La distribución no es lineal \n"
    exit $exitValue
    ;;
esac
