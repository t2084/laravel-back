cd .. && cd machine-learning || exit 1
status=$(git pull origin master) || exit 1

cd cleaningData || exit 1
Rscript script.R

cd .. 
cd RecommendationSystem || exit 1
mvn install
mvn package
echo $status
exit 0
