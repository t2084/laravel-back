cd .. && cd machine-learning || exit 1
status=$(git pull origin master) || exit 1

cd cleaningData || exit 1
Rscript script.R

cd .. && cd RecommendationSystem || exit 1
mvn -T 1C -f pomFiltering.xml install
mvn -T 1C -f pomFiltering.xml package
echo $status
exit 0
